import { Component, OnInit } from '@angular/core';
import { Products } from '../model/products';
import { ActivatedRoute } from '@angular/router';
const productsJson = require('../json/products.json');

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public productsList = [];
  public filterProducts = Products;
  public idSublevel: number;
  private sub: any;
  public check: boolean = false;

  public productsToBuy = [];


  constructor(private route: ActivatedRoute) {
    this.sub = this.route.params.subscribe(params => {
      this.idSublevel = +params['products']; // (+) converts string 'id' to a number
    });
  }

  ngOnInit() {
    this.productsList = productsJson['products'];
    this.productsToBuy = localStorage.getItem("productsToBuy").split(',') ;
  }

  public changePriceToNumber(n: string): number{
    const numberW = n.slice(1, n.length);
    const number = numberW.replace(',','.');
    const double = parseFloat( number )
    return double;
  }

  public filterMin(): void {
    this.productsList.sort((a, b) =>{
      return this.changePriceToNumber(a.price) -this.changePriceToNumber(b.price);
    })
  }

  public filterMax(): void {
    this.productsList.sort((a, b) =>{
      return this.changePriceToNumber(b.price) -this.changePriceToNumber(a.price);
    })
  }

  public selectProduct(product: Products){
    if(product.available){
      if (!this.productsToBuy) {
        this.productsToBuy = [];
      } 
      this.productsToBuy.push(product.id);
      console.log(this.productsToBuy);
      localStorage.setItem("productsToBuy", this.productsToBuy.toString());
    }
  }


  public showProduct(product: Products): boolean {
    return product.sublevel_id === this.idSublevel && ( (product.available == true && this.check) || !this.check) ;
  }

}
