export class Categories {
    public id: number;
    public name: string;
    public sublevels: Categories;
    public showSub: boolean;

    constructor(
        id: number,
        name: string,
        sublevels: Categories,
        showSub: boolean = false
    ){
        this.id = id;
        this.name = name;
        this.sublevels = sublevels;
        this.showSub = showSub;
    }
}
