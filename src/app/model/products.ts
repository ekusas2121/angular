export class Products {
    public id: string;
    public quantity: number;
    public available: boolean;
    public sublevel_id: number;
    public name: string;
    public showProducts: boolean;
    constructor( 
        id: string,
        quantity: number,
        available: boolean,
        sublevel_id: number,
        name: string,
        showProducts: boolean
        ){
            this.id = id;
            this.quantity = quantity;
            this.available = available;
            this.sublevel_id = sublevel_id;
            this.name = name;
            this.showProducts = showProducts;
        }
        sort(){
            
        }
}
