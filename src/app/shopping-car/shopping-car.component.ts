import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Products } from '../model/products';
const productsJson = require('../json/products.json');

@Component({
  selector: 'app-shopping-car',
  templateUrl: './shopping-car.component.html',
  styleUrls: ['./shopping-car.component.css']
})
export class ShoppingCarComponent implements OnInit {

  public productsToBuy = [];
  public productsList = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.productsList = productsJson['products'];
    this.productsToBuy = localStorage.getItem("productsToBuy").split(',');
  }

  public finish(){
    localStorage.setItem("productsToBuy", '[]');
    this.router.navigate(['./home']);

  }

  public showProduct(product): boolean {
    for (let j = 0; j < this.productsToBuy.length; j++) {
      if(!product.items){
        product.items = 1;
      }
      if(!product.realPrice){
        product.realPrice = this.changePriceToNumber(product.price);
      }
      if (product.id == this.productsToBuy[j]) {
        product.isToBuy = true;
        return true;
      }
    }
  }

  public getFullPrice() : number{
    let n: number = 0;
    for (let j = 0; j < this.productsList.length; j++) {
      if(this.productsList[j].isToBuy){
        n += this.productsList[j].realPrice * this.productsList[j].items;
      }
    }
    return n;
  }

  public changePriceToNumber(n: string): number{
    const numberW = n.slice(1, n.length);
    const number = numberW.replace(',','');
    const double = parseFloat( number )
    return double;
  }

}
