import { Component, OnInit } from '@angular/core';
import { Categories } from '../model/categories';
const categories = require('../json/categories.json');

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  public categoriesList: Categories;
  constructor() { }

  ngOnInit() {
    this.categoriesList = categories['categories'];
  }

  public showSubLevel(categorie: Categories): void {
    if(!categorie['showSub']){
      categorie['showSub'] = true;
    } else {
      categorie['showSub'] = false;
    }
  }

}
