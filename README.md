# Angular

Paso 1: Para correr correctamente el proyecto es necesario instalar los paquetes del package con 'npm init'.

Step 1: To init this project you need install the required package first with 'npm init'

Paso 2: Posterior a instalar los paquetes se inicia el servidor que provee angular 'ng serve'

Step: After install required package you can start angular server 'ng serve' 


# Logica

Se procedio a leer bien los requerimientos, y proceder a diseñar y evaluar la mejor metodologia para abordar el problema.

1. Se uso Angular6 (typescript que es javascript potenciado). Bootstrap 4 para mejor visualizacion y para el responsive. Animate css para animaciones. 
2. Se inyectaron los json y se crea una lista con categorias y subcategorias para cuando elija la ultima aparezcan los productos.
3. Se realizan varios componentes para mejor modularidad, entre ellos el de productos, el cual posee los productos referentes a una subcategoria y seleccionarlos para su compra, tambien una funcion para obtener de manera manejable los precios de productos ya que estos vienen en formato "$5,000"

